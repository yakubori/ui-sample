$(function() {
  // Menu handler.
  var $menu = $('#menu-icon');
  var $nav = $('#top-nav');

  $menu.click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $menu.toggleClass('clicked');
    $nav.toggleClass('clicked');
  });

  // Nav link handlers.
  var $links = $('a.nav-link');

  $links.click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $links.removeClass('clicked');
    $(e.target).addClass('clicked');
  });

  // Boroughs handler.
  var $boroughs = $('#boroughs button.btn-regular');

  $boroughs.click(function(e) {
    e.preventDefault();
    e.stopPropagation();

    $(e.target).toggleClass('clicked');
  });

  // Numeric inputs.
  var $nums = $('input.num-input');

  // A very simple, non-regex filter.
  $nums.keyup(function(e) {
    e.stopPropagation();

    var values = e.target.value.split('');
    var keep = [];

    values.forEach(function(v) {
      var num = parseInt(v, 10);

      if ( !isNaN(num) ) {
        keep.push(num);
      }
    });

    e.target.value = keep.join('');
  });
});
