# Objective

This is just a simple UI example. It is intentionally without frameworks such as Bootstrap, Foundation, Material, etc. The goals are to demonstrate some fundamentals in layout and responsiveness but the project can clearly benefit from such libraries.

# CDN

__NB!__ You will need an internet connection to pull in jQuery and Font Awesome.

# Testing

So far, this has been tested in the following browsers:

  - OS X
    - Chrome 52.0.2743.116 (64-bit)
    - Firefox 48.0
    - Safari 9.1.2 (11601.7.7)
